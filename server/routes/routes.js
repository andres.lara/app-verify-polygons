const fs = require('fs');
const inside = require('point-in-polygon');
const express = require('express');
var cors = require('cors');

const app = express();

let optionsCors = {
    allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: '*',
    preflightContinue: false
};

app.use(cors(optionsCors));

app.post('/', function (req, res) {
    let body = req.body;

    if (body.lat === undefined || body.lng === undefined) {
        return res.status(400).json({
            status: false,
            message: 'Falta el campo de lat o lng'
        });
    }
    var coord = [body.lat, body.lng];
    var url = './server/data/file.json';

    fs.readFile(url, function (err, data) {
        if (err) throw err;
        let dataJson = JSON.parse(data);
        for (let el of dataJson) {
            if (inside(coord, el)) {
                return res.json({
                    status: true,
                    latlng: true
                });
            }
        }
        return res.json({
            status: true,
            latlng: false
        });
    });


});

module.exports = app;